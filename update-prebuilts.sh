#!/bin/bash
# based on services/api-daemon/update-prebuilts.sh

set -x -e

BUILD_TYPE=${BUILD_TYPE:-"debug"}
JS_BUILD_TYPE="prod"

if [[ "${BUILD_TYPE}" == "debug" ]]; then
    JS_BUILD_TYPE="build"
elif [[ "${BUILD_TYPE}" == "beta" ]]; then
    BUILD_TYPE="release"
fi

TARGET_TRIPLE=${TARGET_TRIPLE:-armv7-linux-androideabi}

mkdir -p prebuilts/${TARGET_TRIPLE}
cp ./target/${TARGET_TRIPLE}/${BUILD_TYPE}/api-daemon prebuilts/${TARGET_TRIPLE}/api-daemon
# We don't build symbols for all targets
if [[ -d ./target/${TARGET_TRIPLE}/${BUILD_TYPE}/symbols ]]; then
    cp -rf ./target/${TARGET_TRIPLE}/${BUILD_TYPE}/symbols prebuilts/${TARGET_TRIPLE}/
fi

# Update the client side libs and move them to the right place.
BUILD_TYPE=${JS_BUILD_TYPE} RELEASE_ROOT=./prebuilts/http_root/api/v1 ./release_libs.sh
