CLEAN_GECKO = 1
CLEAN_GAIA = 1

all: bundle

services/api-daemon/daemon/config-kaiosrt.toml: services/api-daemon
	cp config-bananian.toml services/api-daemon/daemon/config-kaiosrt.toml

TARGET_TRIPLE=armv7-unknown-linux-gnueabihf
B2G_DIR=$(CURDIR)
MOZCONFIG=$(CURDIR)/mozconfig.bananian
RUSTFLAGS=-Clinker=clang -Clink-args=-fuse-ld=lld -Clink-args=--target=arm-linux-gnueabihf
export B2G_DIR MOZCONFIG RUSTFLAGS TARGET_TRIPLE

.PHONY: bundle
bundle:: gecko gaia services/api-daemon/daemon/config-kaiosrt.toml
	cd gecko && ./mach build $(GECKO_BUILD_ARGS) && ./mach package

.PHONY: api-daemon
api-daemon:: services/api-daemon
	cd services/api-daemon && PKG_CONFIG=arm-linux-gnueabihf-pkg-config cargo build --target=$(TARGET_TRIPLE) $(CARGO_BUILD_ARGS)
	cd services/api-daemon && ../../update-prebuilts.sh

.PHONY: clean
clean::
ifeq ($(CLEAN_GECKO),1)
	cd gecko && ./mach clobber
endif
ifeq ($(CLEAN_GAIA),1)
	$(MAKE) -C gaia clean
endif
